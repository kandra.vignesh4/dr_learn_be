package com.drlearn.DrLearn.Doctor.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.drlearn.DrLearn.Doctor.Entities.Doctor;


public interface DoctorService {

	public List<Doctor> getAllDoctors();

	public Doctor getDoctorById(long id);

	public Doctor addNewDoctor(Doctor doctor);
}
