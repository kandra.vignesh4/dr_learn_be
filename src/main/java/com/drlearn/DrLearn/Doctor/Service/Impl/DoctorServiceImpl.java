
package com.drlearn.DrLearn.Doctor.Service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drlearn.DrLearn.Doctor.DoctorRepository.DoctorRepository;
import com.drlearn.DrLearn.Doctor.Entities.Doctor;
import com.drlearn.DrLearn.Doctor.Service.DoctorService;

@Service
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	DoctorRepository doctorRepo;

	@Override
	public List<Doctor> getAllDoctors() {

		List<Doctor> doctors = new ArrayList<Doctor>();
		doctors = doctorRepo.getAllDoctors();
		return doctors;
	}

	@Override
	public Doctor getDoctorById(long id) {
		Doctor doctor = new Doctor();
		doctor = doctorRepo.getDoctorById(id);
		return doctor;
	}

	@Override
	public Doctor addNewDoctor(Doctor doctor) {
		Doctor doc = new Doctor();
		doc = doctorRepo.save(doctor);
		return doc;
	}

}
