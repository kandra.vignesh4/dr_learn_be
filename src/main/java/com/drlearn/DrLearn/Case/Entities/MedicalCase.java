package com.drlearn.DrLearn.Case.Entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.drlearn.DrLearn.Doctor.Entities.Doctor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicalCase {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "medical_case_id")
	private long caseId;

	@Column(name = "ailment")
	private String ailment;

	private Date admissionDate;
	private Date dischargeDate;

	@OneToOne
	@JoinColumn(name = "patient_id")
	private Patient patient;

	@OneToOne
	@JoinColumn(name = "doctor_id")
	private Doctor doctorIncharge;

	private String treatmentDetails;
	private String observations;

	@OneToMany
	@JoinColumn(name = "case_report_id")
	private List<CaseReport> reports;

}
