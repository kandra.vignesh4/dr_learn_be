package com.drlearn.DrLearn.Case.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaseReport {
	@Id
	@Column(name = "case_report_id")
	private long reportId;
	private String reportName;
	@ManyToOne
	@JoinColumn(name="medical_case_id")
	private MedicalCase caseOfReport;
//Report File
}
