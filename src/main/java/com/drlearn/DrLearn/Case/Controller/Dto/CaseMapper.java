package com.drlearn.DrLearn.Case.Controller.Dto;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.drlearn.DrLearn.Case.Entities.MedicalCase;

@Mapper
public interface CaseMapper {

	CaseMapper MAPPER = Mappers.getMapper(CaseMapper.class);

	@Mappings({

			@Mapping(target = "caseId", source = "medicalCase.caseId"),
			@Mapping(target = "ailment", source = "medicalCase.ailment"),
			@Mapping(target = "admissionDate", source = "medicalCase.admissionDate"),
			@Mapping(target = "dischargeDate", source = "medicalCase.dischargeDate"),
			@Mapping(target = "treatmentDetails", source = "medicalCase.treatmentDetails"),
			@Mapping(target = "observations", source = "medicalCase.observations"),
			@Mapping(target = "reports", source = "medicalCase.reports"),
			@Mapping(target = "patientId", source = "medicalCase.patient.patientId"),
			@Mapping(target = "patientName", source = "medicalCase.patient.patientName"),
			@Mapping(target = "doctorId", source = "medicalCase.doctorIncharge.doctorId"),
			@Mapping(target = "doctorName", source = "medicalCase.doctorIncharge.doctorName")

	})
	CaseDto convertToDto(MedicalCase medicalCase);

	MedicalCase convertToEntity(CaseDto casedto);

	/*
	 * @Mappings({ @Mapping(target = "patientId", source = "Patient.ailment"),
	 * 
	 * @Mapping(target = "patientName", source = "Patient.ailment"),
	 * 
	 * })
	 * 
	 * CaseDto convertPatientToDto(Patient doctor);
	 * 
	 * Patient convertCaseDtoToPatientEntity(CaseDto casedto);
	 * 
	 * @Mappings({ @Mapping(target = "doctorId", source = "medicalCase.ailment"),
	 * 
	 * @Mapping(target = "doctorName", source = "medicalCase.ailment") })
	 * 
	 * CaseDto convertDoctorToDto(Doctor doctor);
	 * 
	 * Doctor convertCaseDtoToDoctorEntity(CaseDto casedto);
	 */

}
