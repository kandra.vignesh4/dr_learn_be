package com.drlearn.DrLearn.Case.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.drlearn.DrLearn.Case.Controller.Dto.CaseDto;
import com.drlearn.DrLearn.Case.Controller.Dto.CaseMapper;
import com.drlearn.DrLearn.Case.Entities.MedicalCase;
import com.drlearn.DrLearn.Case.Repository.CaseRepository;
import com.drlearn.DrLearn.Case.Service.CaseService;

@Service
public class CaseServiceImpl implements CaseService {

	@Autowired
	CaseRepository caseRepo;

	@Override
	public List<MedicalCase> getAllCases() {

		return caseRepo.getAllCases();
	}

	@Override
	public MedicalCase getCaseById(long id) {

		return caseRepo.getCaseById(id);
	}

	@Autowired
	CaseMapper caseMapper;

	@Override
	public MedicalCase addCase(CaseDto casebody) {
		MedicalCase medicalCase = new MedicalCase();
		medicalCase = caseMapper.convertToEntity(casebody);

		return caseRepo.save(medicalCase);
	}

	@Override
	public void deleteCase(long caseId) {
		caseRepo.deleteById(caseId);

	}

}
