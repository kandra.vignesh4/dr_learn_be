package com.drlearn.DrLearn.Doctor.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.drlearn.DrLearn.Case.Entities.CaseReport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Doctor {
	@Id
	@Column(name = "doctor_id")
	private long doctorId;
	private String doctorName;
	private String emailId;
	private long contactNumber;
	private String socialMediaLink;
}
