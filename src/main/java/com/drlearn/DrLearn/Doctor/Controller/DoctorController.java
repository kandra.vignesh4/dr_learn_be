package com.drlearn.DrLearn.Doctor.Controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.drlearn.DrLearn.Doctor.Entities.Doctor;
import com.drlearn.DrLearn.Doctor.Service.DoctorService;

@RestController
@RequestMapping("/Doctor")
public class DoctorController {

	@Autowired
	DoctorService doctorService;

	// API to Add Doctor

	@PostMapping("/add")
	@ResponseBody
	public ResponseEntity<?> addNewDoctor(@RequestBody Doctor doctor) {

		ResponseEntity<?> response;
		response = new ResponseEntity<Doctor>(doctorService.addNewDoctor(doctor), HttpStatus.OK);

		return response;
	}

	// API to view DoctorById
	@GetMapping("/{id}")
	@ResponseBody
	public ResponseEntity<?> getDoctorById(@PathVariable("id") long id) {
		ResponseEntity<?> response;

		response = new ResponseEntity<Doctor>(doctorService.getDoctorById(id), HttpStatus.OK);

		return response;
	}

	// API to view List of all Doctors

	@GetMapping
	@ResponseBody
	public List<Doctor> getAllDoctors() {

		List<Doctor> doctors = new ArrayList<Doctor>();
		doctors = doctorService.getAllDoctors();
		return doctors;
	}

}
