package com.drlearn.DrLearn.Doctor.Entities;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.drlearn.DrLearn.Case.Entities.CaseReport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageToDoctor {

	@Id
	private long messageId;
	private String messageSubject;
	private String message;
}
