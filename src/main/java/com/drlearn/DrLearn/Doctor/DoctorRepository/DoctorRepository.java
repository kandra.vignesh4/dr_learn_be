package com.drlearn.DrLearn.Doctor.DoctorRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.drlearn.DrLearn.Doctor.Entities.Doctor;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

	@Query(value = "SELECT d FROM Doctor d")
	public List<Doctor> getAllDoctors();

	@Query(value = "SELECT d FROM Doctor d WHERE d.doctorId=:id")
	public Doctor getDoctorById(@Param("id") long id);
}
