package com.drlearn.DrLearn.Case.Service;

import java.util.List;

import com.drlearn.DrLearn.Case.Controller.Dto.CaseDto;
import com.drlearn.DrLearn.Case.Entities.MedicalCase;

public interface CaseService {

	public List<MedicalCase> getAllCases();

	public MedicalCase getCaseById(long id);

	public MedicalCase addCase(CaseDto casebody);

	public void deleteCase(long caseId);

}
