package com.drlearn.DrLearn.Case.Controller.Dto;

import java.util.Date;
import java.util.List;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.drlearn.DrLearn.Case.Entities.CaseReport;
import com.drlearn.DrLearn.Case.Entities.Patient;
import com.drlearn.DrLearn.Doctor.Entities.Doctor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CaseDto {

	private long caseId;

	private String ailment;

	private Date admissionDate;
	private Date dischargeDate;

	// click on view patient, the based on patient id we get the details of patient
	// id
	private long patientId;
	private long patientName;

	// click on view doctor, the based on doctor id we get the details of doctor id
	private long doctorId;
	private long doctorName;

	private String treatmentDetails;
	private String observations;

	private List<CaseReport> reports;

}
