package com.drlearn.DrLearn.Case.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.drlearn.DrLearn.Case.Controller.Dto.CaseDto;
import com.drlearn.DrLearn.Case.Entities.MedicalCase;
import com.drlearn.DrLearn.Case.Repository.CaseRepository;
import com.drlearn.DrLearn.Case.Service.CaseService;

@RestController
@RequestMapping("/Case")
public class CaseController {

	@Autowired
	CaseService caseService;

	@Autowired
	CaseRepository caseRepo;

	@GetMapping
	@ResponseBody
	public List<MedicalCase> getAllCases() {

		return caseService.getAllCases();
	}

	@GetMapping("/{id}")
	@ResponseBody
	public ResponseEntity<?> getCaseById(@PathVariable("id") long id) {
		ResponseEntity<?> response;
		response = new ResponseEntity<MedicalCase>(caseService.getCaseById(id), HttpStatus.OK);
		return response;
	}

	@PostMapping("/add")
	@ResponseBody
	public ResponseEntity<?> addNewNewCase(CaseDto newCase) {
		ResponseEntity<?> response;
		response = new ResponseEntity<MedicalCase>(caseService.addCase(newCase), HttpStatus.OK);

		return response;
	}

	@DeleteMapping("/delete/{id}")
	@ResponseBody
	public void deleteCaseById(@PathVariable("id") long id) {
		caseService.deleteCase(id);
	}

}
