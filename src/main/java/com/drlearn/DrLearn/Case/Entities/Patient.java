package com.drlearn.DrLearn.Case.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Patient {

	@Id
	@Column(name = "patient_id")
	private long patientId;
	private String patientName;
	private int patientAge;
	private long patientContactNumber;

}
