package com.drlearn.DrLearn.Case.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.drlearn.DrLearn.Case.Entities.MedicalCase;

@Repository
public interface CaseRepository extends JpaRepository<MedicalCase, Long> {

	@Query(value = "SELECT c FROM MedicalCase c")
	public List<MedicalCase> getAllCases();

	@Query(value = "SELECT c from MedicalCase c WHERE c.caseId =:id")
	public MedicalCase getCaseById(@Param("id") long id);
}
