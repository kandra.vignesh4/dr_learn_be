package com.drlearn.DrLearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.drlearn.DrLearn.Case.Controller.Dto.CaseMapper;
import com.drlearn.DrLearn.Case.Controller.Dto.CaseMapperImpl;

@SpringBootApplication
public class DrLearnApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrLearnApplication.class, args);
	}

	@Bean
	public CaseMapper mapper() {
		return new CaseMapperImpl();
	}
}
